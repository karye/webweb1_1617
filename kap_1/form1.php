<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="">
</head>

<body>
    <form action="person.php">
        <label>Förnamn</label>
        <input type="text" name="fnamn">
        <br>
        <label>Efternamn</label>
        <input type="text" name="enamn">
        <br>
        <label>Adress</label>
        <input type="text" name="adress">
        <br>

        <label>Myndig</label>
        <input type="radio" value="ja" name="myndig" checked>Ja
        <input type="radio"value="nej" name="myndig">Nej
        <br>

        <label>Favoritbil</label>
        <select name="bil">
            <option value="volvo">Volvo</option>
            <option value="saab">Saab</option>
            <option value="fiat">Fiat</option>
            <option value="audi">Audi</option>
        </select>
        <br>

        <label>Kommentar</label>
        <textarea name="kommentar"></textarea><br>

        <input type="submit">
    </form>
</body>

</html>
