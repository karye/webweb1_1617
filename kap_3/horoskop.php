<?php
/**
* Hämtar dagens horoskop från metro.se
*
* PHP version 5
* @category   Enkel webscrapping
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/

// Tar emot skickat data
$url = $_POST['url'];
$sign = $_POST['sign'];

// Ladda hem hela webbsidan från metro
$sida = file_get_contents($url);

// Leta rätt på var horoskoptexten finns
$hittat = strpos($sida, $sign);

if ($hittat != false) {
    echo "<p>$hittat</p>";

    // Letat rätt på slutet på texten
    $slut = strpos($sida, "</p>", $hittat);
    //var_dump($slut);

    // Plocka ut horoskoptexten från start till slut
    $horoskopet = substr($sida, $hittat, $slut-$hittat);
    echo "<p>$horoskopet</p>";
}
?>
