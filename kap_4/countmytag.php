<?php
/**
* Räkna hur många gånger min tag
* förekommer i inskickad url
* Använder sig av http://simplehtmldom.sourceforge.net/
*
* PHP version 5
* @category   Räkna specifik tag
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/

include "funktioner.php";
include "simple_html_dom.php";
?>

<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Räkna mitt ord i en text</title>
    <link rel="stylesheet" href="">
</head>
<body>
    <h1>Räkna ordet i texten</h1>
    <?php
    // Om post-variabeln "text" finns då läser vi av den
    if (isset($_POST["url"]) && isset($_POST["tag"])) {
        $url = $_POST["url"];
        $tag = $_POST["tag"];

        // Parsa HTML på adressen url
        $html = file_get_html($url);
        $count = 0;

        // Leta efter en tag
        foreach($html->find($tag) as $element) {
            $count++;
        }

        rubrik($count);
    }
    ?>
    <form method="post">
        <input type="text" name="url"><br>
        <input type="text" name="tag"><br>
        <input type="submit" value="Räkna">
    </form>
</body>
</html>

