<?php
/**
* Räkna hur många gånger mitt ord
* förekommer i inskickad text
*
* PHP version 5
* @category   Räkna specifikt ord
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/

include "funktioner.php";
?>

<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Räkna mitt ord i en text</title>
    <link rel="stylesheet" href="">
</head>
<body>
    <h1>Räkna ordet i texten</h1>
    <?php
    // Om post-variabeln "text" finns då läser vi av den
    if (isset($_POST["text"]) && isset($_POST["ordet"])) {
        $text = $_POST["text"];
        $ordet = $_POST["ordet"];
        $antalOrd = countMyWord($text, $ordet);
        rubrik($antalOrd);
    }
    ?>
    <form method="post">
        <textarea name="text"></textarea><br>
        <input type="text" name="ordet"><br>
        <input type="submit" value="Räkna">
    </form>
</body>
</html>
