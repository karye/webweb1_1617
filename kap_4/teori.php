<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="">
</head>
<body>
    <?php
    function rubrik($text) {
        echo "<h1>$text</h1>";
    }
    rubrik("Tomte");

    function färgadText($text, $färg) {
        echo "<h2 style=\"color:$färg\">$text</h2>";
    }
    färgadText("Oktober", "red");
    färgadText("November", "purple");
    färgadText("December", "green");

    $antalOrd = str_word_count("Vi tar studenten nästa år 2017!");
    rubrik($antalOrd);
    ?>
</body>
</html>
