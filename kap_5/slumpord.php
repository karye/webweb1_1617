
<?php
/**
* Slumpar fram ett ordspråk
*
* PHP version 5
* @category   Enkel skriptsida
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>

<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Logga in</title>
</head>
<body>
<?php

    $ordsprak[] = "Blyga pojkar får aldrig kyssa vackra flickor.";
    $ordsprak[] = "Borta bra men hemma bäst.";
    $ordsprak[] = "Bra karl reder sig själv.";
    $ordsprak[] = "Bränt barn skyr elden.";
    $ordsprak[] = "Bättre att förekomma än förekommas.";
    $ordsprak[] = "Bättre brödlös än rådlös.";
    $ordsprak[] = "Bättre en fågel i handen än tio i skogen.";
    $ordsprak[] = "Bättre ensam än i dåligt sällskap.";
    $ordsprak[] = "Bättre fly än illa fäkta.";
    $ordsprak[] = "Bättre föregå än föregås.";
    $kontrol[] = array();

    // for-loop som går 6 varv
    for ($i = 1; $i < 6; $i++) {

        // Slumpa ut ett tal mellan 0 och 9
        $index = rand(0, 9);

        // Skriv om inte finns i arrayen $kontroll
        if (in_array($index, $kontrol) == false) {
            echo "<p>$ordsprak[$index]</p>";
            $kontrol[] = $index;
        } else {
            $i--;
        }
    }

?>
</body>
</html>
