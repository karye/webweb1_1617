<?php
/**
* Loginsida som kollar upp användare i en textfil
*
* PHP version 5
* @category   Enkel skriptsida
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>

<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Logga in</title>
</head>

<body>
    <?php
    $inloggad = false;

    if (isset($_POST["user"]) && isset($_POST["pass"])) {
        $user = $_POST["user"];
        $pass = $_POST["pass"];

        // Rensa bort mellanslag i början och slutet
        $user = trim($user);
        $pass = trim($pass);

        // Omvandla till små bokstäver
        $user = strtolower($user);
        $pass = strtolower($pass);

        // Vi sparar ned användarnamn och lösenord i en textfil
//        $fil = fopen("C:/xampp/htdocs/users.txt", "r") or die("Kunde inte öppna fil");

        // Loopa rad för rad och kolla om användare och lösenordet finns
//        if ($fil) {
//            while (($rad = fgets($fil, 4096)) !== false) {
//                $rad = trim($rad);
//
//                // Dela upp raden i användarnamn och hash
//                $ord = explode(':', $rad); // ord[0]=user och ord[1]=hash
//
//                if ($user == $ord[0] && password_verify($pass, $ord[1])) {
//                    $inloggad = true;
//                }
//            }
//            if (!feof($fil)) {
//                echo "Error: unexpected fgets() fail\n";
//            }
//            fclose($fil);
//        }

        // Läs in hela filen i en array
        $rader = file("C:/xampp/htdocs/users.txt");
        foreach ($rader as $index=>$rad) {
            echo "<p>rad=$rad index=$index</p>";
            $rad = trim($rad);
            if ($rad == "user=$user") {
                echo "<p>Hittat: rad=$rad index=$index</p>";
                // Hämta raden efter med lösenordshashet
                $radEfter = $rader[$index+1];
                // Plocka ut hashet
                $hash = substr($radEfter, 9);
                // Städa bort skräptecken
                $hash = trim($hash);
                // Kolla om lösenordet stämmer
                if (password_verify($pass, $hash)) {
                    $inloggad = true;
                }
            }
        }
    }
    if (!$inloggad) {
        if (isset($user) && isset($pass)) {
            echo "<h1>Fel användarnamn eller lösenord! Försök igen.</h1>";
        }
        ?>
        <form method="post">
            <h2>Logga in användare</h2>
            <label>Username: </label><input type="text" name="user"><br>
            <label>Password: </label><input type="password" name="pass"><br>
            <button type="submit">Logga in</button>
        </form>
        <?php
    } else {
        echo "<h1>Du är nu inloggad!</h1>";
    }
    ?>
</body>

</html>
