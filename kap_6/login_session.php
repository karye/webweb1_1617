<?php
/**
* Loginsida som kollar upp användare i en textfil
*
* PHP version 5
* @category   Enkel skriptsida
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>

<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Logga in</title>
</head>

<body>
    <?php
    $inloggad = false;

    if (isset($_POST["user"]) && isset($_POST["pass"])) {
        $user = $_POST["user"];
        $pass = $_POST["pass"];

        // Rensa bort mellanslag i början och slutet
        $user = trim($user);
        $pass = trim($pass);

        // Omvandla till små bokstäver
        $user = strtolower($user);
        $pass = strtolower($pass);

        // Läs in hela filen i en array
        $rader = file("C:/xampp/htdocs/users.txt");
        foreach ($rader as $index=>$rad) {
            echo "<p>rad=$rad index=$index</p>";
            $rad = trim($rad);
            if ($rad == "user=$user") {
                echo "<p>Hittat: rad=$rad index=$index</p>";
                // Hämta raden efter med lösenordshashet
                $radEfter = $rader[$index+1];
                // Plocka ut hashet
                $hash = substr($radEfter, 9);
                // Städa bort skräptecken
                $hash = trim($hash);
                // Kolla om lösenordet stämmer
                if (password_verify($pass, $hash)) {
                    session_start();
                    $_SESSION['user'] = $user;
                }
            }
        }
    }
    if (!isset($_SESSION['user'])) {
        if (isset($user) && isset($pass)) {
            echo "<h1>Fel användarnamn eller lösenord! Försök igen.</h1>";
        }
        ?>
        <form method="post">
            <h2>Logga in användare</h2>
            <label>Username: </label><input type="text" name="user"><br>
            <label>Password: </label><input type="password" name="pass"><br>
            <button type="submit">Logga in</button>
        </form>
        <?php
    } elseif (isset($_SESSION['user'])) {
    ?>
        <form method="post">
            <button type="submit" name="logout">Logga ut</button>
        </form>
    <?php
        if (isset($_POST['logout'])) {
            session_destroy();
        }
    }
    ?>
</body>

</html>
