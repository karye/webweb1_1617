<?php
/**
* Gör ett formulär där användaren ska fylla i namn, adress, postnr och postort.
* Kontrollera att alla fälten är ifyllda, och innehåller minst 3 tecken.
* Kontrollera att postnumret innehåller 5 tecken och att de tecknen endast är siffror.
*
* PHP version 5
* @category   Enkel skriptsida
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>...</title>
</head>
<body>
    <?php
    if (isset($_POST["namn"]) && isset($_POST["adress"]) &&
        isset($_POST["postnr"]) && isset($_POST["postort"])) {
        $namn = $_POST["namn"];
        $adress = $_POST["adress"];
        $postnr = $_POST["postnr"];
        $postort = $_POST["postort"];

        // Kontrollera att alla fälten är ifyllda
        if (strlen($namn) > 0 && strlen($adress) > 0 && strlen($postnr) > 0 && strlen($postort) > 0)
            echo "<p>Yeap! Alla fält är ifyllda.</p>";
        else
            echo "<p>Nope! En eller flera fält är tomma, vg försök igen!</p>";

        // Kontrollera att alla fält innehåller minst 3 tecken
        if (strlen($namn) > 2 && strlen($adress) > 2 && strlen($postnr) > 2 && strlen($postort) > 2)
            echo "<p>Yeap! Alla fält har minst 3 tecken.</p>";
        else
            echo "<p>Nope! En eller flera fält är mindre är 3 tecken, vg försök igen!</p>";

        // Kontrollera att postnumret innehåller 5 tecken
        if (strlen($postnr) == 5)
            echo "<p>Yeap! Postnumret innehåller 5 tecken</p>";
        else
            echo "<p>Nope! postnumret innehåller färre eller fler än 5 tecken</p>";

        // Kontrollera att postnumret endast innehåller enbart siffror
        if (ctype_digit($postnr) )
            echo "<p>Yeap! Postnumret innehåller enbart siffror</p>";
        else
            echo "<p>Yeap! Postnumret innehåller inte enbart siffror</p>";

    } else {
    ?>
    <form method="post">
        <h2>Logga in användare</h2>
        <label>Namn: </label><input type="text" name="namn"><br>
        <label>Adress: </label><input type="text" name="adress"><br>
        <label>PostNr: </label><input type="text" name="postnr"><br>
        <label>PostOrt: </label><input type="text" name="postort"><br>
        <input type="submit">
    </form>
    <?php
    }
    ?>
</body>
</html>
