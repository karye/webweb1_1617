<?php
/**
* Registrera användare och lagra i en textfil
*
* PHP version 5
* @category   Enkel skriptsida
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>

<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Registrera användare</title>
</head>

<body>
    <?php
    if (isset($_POST["user"]) && isset($_POST["pass"]) && !empty($_POST["user"]) && !empty($_POST["pass"])) {
        $user = $_POST["user"];
        $pass = $_POST["pass"];

        // Rensa bort mellanslag i början och slutet
        $user = trim($user);
        $pass = trim($pass);

        // Omvandla till små bokstäver
        $user = strtolower($user);
        $pass = strtolower($pass);

        // Kolla om användarnamnet redan finns
        $userRegistered = false;
        $fil = fopen("C:/xampp/htdocs/users.txt", "r") or die("Kunde inte öppna fil");

        // Loopa rad för rad och kolla om användare redan finns
        if ($fil) {
            while (($rad = fgets($fil, 4096)) !== false) {
                $rad = trim($rad);
                if ($rad == "user=$user") {
                    echo "<p>Användarnamnet är upptaget vg försök igen!</p>";
                    $userRegistered = true;
                    break;
                }
            }
            fclose($fil);
        }

        // Vi sparar ned användarnamn och lösenord i en textfil
        if (!$userRegistered) {
            $fil = fopen("C:/xampp/htdocs/users.txt", "a+") or die("Kunde inte öppna fil");
            $hash = password_hash($pass, PASSWORD_DEFAULT);
            fwrite($fil, "user=$user\n");
            fwrite($fil, "password=$hash\n");
            fclose($fil);
        }
    }
    ?>
    <form method="post">
        <h2>Registrera användare</h2>
        <label>Username: </label><input type="text" name="user"><br>
        <label>Password: </label><input type="password" name="pass"><br>
        <button type="submit">Skicka</button>
    </form>
</body>

</html>
