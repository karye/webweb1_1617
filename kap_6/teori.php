<?php
/**
*
*
* PHP version 5
* @category   Enkel skriptsida
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>

<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Stränghantering</title>
</head>

<body>
    <?php
    if (isset($_POST["adress"])) {
        $adress = $_POST["adress"];
        echo "<p>.$adress.</p>";

        // Rensa bort mellanslag i början och slutet
        $adress = trim($adress);
        echo "<p>.$adress.</p>";

        if (strstr($adress, ':')) {
            echo 'Variabeln $adress innehåller kolon';
        } else {
            echo 'Variabeln $adress innehåller inte kolon';
        }
    }
    ?>
    <form method="post">
        <input type="text" name="adress"><br>
        <input type="submit">
    </form>
</body>

</html>
