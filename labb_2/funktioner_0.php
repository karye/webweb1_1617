<?php
/**
* Samling bra att ha funktioner
*
* PHP version 5
* @category   Funktioner
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/

// Skapa en funktion som skriver en text som en rubrik
??? rubrik($text) {
    echo "<h1>$text</h1>";
}

