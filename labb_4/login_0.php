<?php
/**
* Loginsida som kollar upp användare i en textfil
*
* PHP version 5
* @category   Enkel skriptsida
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>

<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Logga in</title>
</head>

<body>
    <?php
    // Flagga som registrerar om inloggad eller ej
    $inloggad = false;

    // Kontrollera först om det finns formulärdata annars visa inloggningsformuläret
    if (??? && ???) {
        $user = ???
        $pass = ???

        // Rensa bort mellanslag i början och slutet
        $user = ???
        $pass = ???

        // Omvandla till små bokstäver
        $user = ???
        $pass = ???

        // Läser in hela filen "C:/xampp/htdocs/users.txt" i en array
        $rader = ???(???);

        // Loopa igenom arrayen
        ??? (??? as ???) {

            // Rensa bort skräptecken som osynligt radslut
            $rad = ???

            // Dela upp raden i användarnamn och hash med funktionen explode
            $ord = ??? // ord[0]=user och ord[1]=hash
            $namn = ???
            $hash = ???

            // Kollar om inmattade användarnamn är stämmer
            // och lösenordet stämmer med password_verify
            if (??? && ???) {
                $inloggad = true;
            }

        }
    }

    // Om användaren inte är inloggad
    if (!$inloggad) {

        // Om obefintligt formulärdata
        if (??? && ???) {
            echo "<h1>Fel användarnamn eller lösenord! Försök igen.</h1>";
        }
        ?>
        <form method="post">
            <h2>Logga in användare</h2>
            <label>Username: </label><input type="text" name="user"><br>
            <label>Password: </label><input type="password" name="pass"><br>
            <input type="submit">
        </form>
        <?php
    } else {
        echo "<h1>Du är nu inloggad!</h1>";
    }
    ?>
</body>

</html>
