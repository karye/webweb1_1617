<?php
/**
* Registrera användare och lagra i en textfil
*
* PHP version 5
* @category   Enkel skriptsida
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>

<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Registrera användare</title>
</head>

<body>
    <?php
    // Kolla att formulärdata finns annars visa formuläret
    if (??? && ???) {

        // Hämta ut formulärdata
        $user = ???
        $pass = ???

        // Rensa bort mellanslag i början och slutet
        $user = ???
        $pass = ???

        // Omvandla till små bokstäver
        $user = ???
        $pass = ???

        // Vi sparar ned användarnamn och lösenord i en textfil
        // Öppna textfilen "C:/xampp/htdocs/users.txt" för skrivning
        $fil = fopen(???) or die("Kunde inte öppna fil");

        // Skapa hash av lösenordet med funktionen password_hash
        $hash = ???;

        // Skriv en ny rad med användarnamn och lösenord i formatet "user=xxx:password=xxx"
        fwrite($fil, "???\n");

        // Stäng textfilen
        ???;
    }
    ?>
    <form method="post">
        <h2>Registrera användare</h2>
        <label>Username: </label><input type="text" name="user"><br>
        <label>Password: </label><input type="password" name="pass"><br>
        <input type="submit">
    </form>
</body>

</html>
