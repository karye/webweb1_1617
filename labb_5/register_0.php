<?php
/**
* Registrera användare och lagra i en textfil
*
* PHP version 5
* @category   Enkel skriptsida
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>

<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Registrera användare</title>
</head>

<body>
    <?php

    // Kontrollerar att formulärdata finns och ej är tomt med funktionerna isset och inte empty
    if (??? && ??? &&
        ??? && ???) {

        // Plockar ut formulärdata
        $user = ???
        $pass = ???

        // Rensa bort mellanslag i början och slutet
        $user = ???;
        $pass = ???;

        // Omvandla till små bokstäver
        $user = ???
        $pass = ???

        // Kolla om användarnamnet redan finns
        $userRegistered = false;

        // Läs in hela filen i en array med funktionen file
        $rader = ???("C:/xampp/htdocs/users.txt");

        // Loopa igenom arrayen
        ??? (??? as $rad) {

            // Rensa bort tomma tecken
            $rad = ???;

            // Kollar om användarnamn redan finns
            if ($rad == ???) {
                echo "<p>Användarnamnet är upptaget vg försök igen!</p>";
                $userRegistered = true;
                break;
            }
        }

        // Vi sparar ned användarnamn och lösenord i en textfil
        if (!$userRegistered) {

            // Öppna textfilen för att spara
            $fil = ???("C:/xampp/htdocs/users.txt", "a+") or die("Kunde inte öppna fil");

            // Ta fram hash på lösenordet
            $hash = ???(???, PASSWORD_DEFAULT);

            // Skriv en rad med användarnamn
            ???($fil, "user=$user\n");

            // Skriv en rad med hash
            ???($fil, "password=$hash\n");

            // Stäng filen
            ???;
        }
    }
    ?>
    <form method="post">
        <h2>Registrera användare</h2>
        <label>Username: </label><input type="text" name="user"><br>
        <label>Password: </label><input type="password" name="pass"><br>
        <input type="submit">
    </form>
</body>

</html>
