<?php
session_start();
?>
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Ladda upp filer</title>
</head>
<body>
<?php
if (isset($_SESSION['uploaded'])) {
    echo "<p>Filen {$_SESSION['uploaded']} har laddats upp!</p>";
    session_destroy();
}
?>
    <form action="upload.php" method="post" enctype="multipart/form-data">
        <input type="file" name="file"><br>
        <button type="submit" name="submit">Ladda upp</button>
    </form></body>
</html>
