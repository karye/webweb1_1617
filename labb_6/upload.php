<?php
/**
* Ladda upp filer
*
* PHP version 5
* @category   Enkel filuppladdning
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/

session_start();

if (isset($_POST['submit'])) {
    $file = $_FILES['file'];
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileSize = $_FILES['file']['size'];
    $fileError = $_FILES['file']['error'];
    $fileType = $_FILES['file']['type'];

    $fileExt = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
    $allowedExt = ['jpg', 'jpeg', 'png'];
    if (in_array($fileExt, $allowedExt)) {
        if ($fileError === 0) {
            if ($fileSize < 500000) {
                $fileNameNew = uniqid('', true) . "." . $fileExt;
                $fileDestination = "uploads/$fileNameNew";

                $_SESSION['uploaded'] = $fileName;
                move_uploaded_file($fileTmpName, $fileDestination);
                header("Location: index.php");
            } else {
                echo "<p>Fel! Filen är större än 500KB</p>";
            }
        } else {
            echo "<p>Det blev ett fel. Vg försök igen!</p>";
        }
    } else {
        echo "<p>Fel! Du får inte ladda filer med filtyp $fileExt.</p>";
    }


}
